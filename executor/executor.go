// Package executor handles all buying and selling transactions while
// keeping track of everything in the database.
package executor

import (
	"log"
	"reflect"
	"strconv"
	"time"

	db "../database"
	"../integrations"
)

// sellingThreshold is the minimum a position must have gained before it
// is eligable for being sold.
// A selling threshold of 0.03 means that the value must have gained at
// least 3% in value.
const sellingThreshold = 0.03

// sellingThresholdMultiplier defines how much the price must drop from its
// maximum value before we start bying new positions
const sellingThresholdMultiplier = 3

// defaultBuyingValueInEUR defines the value of a new position in EUR. Only
// if the account has enough EUR funds a new position can be bought
const defaultBuyingValueInEUR = 50

// buyingThreshold sets the value at which to by a new position of any given
// currency
var buyingThresholdMapping = make(map[string]float64)

// currentValueMapping sets the current value of each currency
var currentValueMapping = make(map[string]float64)

// Init sets up the executor and starts its subroutine timer
func Init(exchange integrations.ExchangeIntegration, emergencyChannel <-chan string) {
	log.Println("executor::Init()")

	quitRoutine := make(chan bool)
	go startExecutionTimer(exchange, quitRoutine)
	go startAccountBalanceTracker(exchange)

	select {
	case emergencyMessage := <-emergencyChannel:
		if emergencyMessage == "stop" {
			quitRoutine <- true
		}
	}
}

func handleExecution(exchange integrations.ExchangeIntegration) {
	log.Println("executor::handleExecution()")

	var activeCurrencies = []string{"XRP"}
	var maxValue float64

	for _, currency := range activeCurrencies {
		log.Println("Currency to handle:", currency)

		//
		// Retrieve latest market prices
		//

		var currentPrice float64
		if currencySnapshot := exchange.GetCurrencySnapshot(currency); currencySnapshot != nil {
			currentPrice = currencySnapshot.Current
		} else {
			log.Println("Currency snapshot could not be retrieved")
			return
		}

		log.Println("Current price:", currentPrice)

		//
		// Define the buying threshold
		//

		var buyingThreshold float64
		if val, ok := buyingThresholdMapping[currency]; ok {
			buyingThreshold = val
		} else {
			buyingThreshold = currentPrice * (1 - sellingThreshold)
		}

		//
		// Get the maximum value the currency has reached in recent times
		//

		maxValueString := db.GetParameter(currency + "_max")
		maxValueMustBeResetted := false

		if maxValueString == "" {
			log.Println("No max value given in the database")
			maxValue = currentPrice
			maxValueMustBeResetted = true
		} else {
			var err error
			if maxValue, err = strconv.ParseFloat(maxValueString, 64); err != nil {
				log.Println("The max value string cannot be parsed")
				maxValue = currentPrice
				maxValueMustBeResetted = true
			} else {
				// if the max value has plummeted by at least three times the
				// selling threshold, it must be reset to the current price
				if maxValue > (1+3*sellingThreshold)*currentPrice {
					log.Println("The currency price plummeted")
					maxValue = currentPrice
					maxValueMustBeResetted = true
				}
			}
		}

		// reset the max value in case the current value is higher
		if currentPrice > maxValue {
			log.Println("The current price is higher than the max value")
			maxValue = currentPrice
			maxValueMustBeResetted = true
		}

		if maxValueMustBeResetted {
			log.Println("Max value:", maxValue)

			if changeSettingErr := db.ChangeSetting(currency+"_max", strconv.FormatFloat(maxValue, 'f', -1, 64)); changeSettingErr != nil {
				log.Println("Could not change max value setting in database:", changeSettingErr)
			}
		}

		//
		// Handle selling of existing positions
		//

		orders, err := exchange.GetOpenOrders(currency)
		if err != nil {
			log.Println("Could not retrieve open orders:" + err.Error())
		}

		log.Println("Open orders:", orders)

		if positions, err := db.GetTradingPositions(currency); err == nil {
			log.Println("Trading positions", positions)

			for _, position := range positions {
				if position.RelatedOrder > 0 {
					// there is an order related to that position
					var orderIsAlreadyExecuted = true

					if orders == nil {
						// orders could not be retrieved
						orderIsAlreadyExecuted = false
					}

					for _, order := range orders {
						if order.ID == position.RelatedOrder {
							orderIsAlreadyExecuted = false
						}
					}

					if orderIsAlreadyExecuted {
						if position.OrderType == "S" {
							if err := db.RemoveTradingPosition(position.ID); err != nil {
								log.Panicln("Could not delete trading position from database:", err.Error())
							}

							transaction := db.Transaction{
								Type:   "S",
								Amount: position.Amount,
								Value:  position.Value}

							if createTransactionErr := db.StoreTransaction(position.Currency, &transaction); createTransactionErr != nil {
								log.Fatalln("Could not create transaction for selling position.")
							}
						} else if position.OrderType == "B" {
							if err := db.UpdateTradingPosition(position.ID, 0, ""); err != nil {
								log.Panicln("Could not update trading position:", err.Error())
							}

							transaction := db.Transaction{
								Type:   "B",
								Amount: position.Amount,
								Value:  position.Value}

							if createTransactionErr := db.StoreTransaction(position.Currency, &transaction); createTransactionErr != nil {
								log.Fatalln("Could not create transaction for buying position.")
							}
						}
					}
				} else {
					// no order has been created yet
					if position.UnitPrice*(1+sellingThreshold) < currentPrice {
						newSellingOrder := executeSalesOrder(exchange, position, currentPrice)

						if newSellingOrder == nil {
							log.Println("Could not create new sales order")
							return
						}

						if err := db.UpdateTradingPosition(position.ID, newSellingOrder.ID, "S"); err != nil {
							log.Panicln("Could not update trading position:", err.Error())
						}

						// reset buying threshold
						buyingThresholdMapping[currency] = currentPrice * (1 - 2*sellingThreshold)
					}
				}
			}
		} else {
			log.Println("Could not retrieve trading positions from database:", err)
		}

		//
		// Handle buying of new positions
		//

		if buyingThreshold >= currentPrice || currentPrice < maxValue*(1-sellingThresholdMultiplier*sellingThreshold) {
			// check if new position can be bought
			if accountBalance := exchange.GetAccountBalance(); accountBalance != nil {
				log.Println("Available funds (EUR):", accountBalance.EUR)

				if accountBalance.EUR >= defaultBuyingValueInEUR {
					newPurchaseOrder := executePurchaseOrder(exchange, currency, defaultBuyingValueInEUR, currentPrice)

					if newPurchaseOrder == nil {
						log.Println("Could not create new purchase order")
						return
					}

					newTradingPosition := db.TradingPosition{
						Currency:     currency,
						Amount:       newPurchaseOrder.Amount,
						UnitPrice:    newPurchaseOrder.Price,
						Value:        defaultBuyingValueInEUR,
						Timestamp:    time.Now().Unix(),
						RelatedOrder: newPurchaseOrder.ID,
						OrderType:    "B"}

					if err := db.AddTradingPosition(&newTradingPosition); err != nil {
						log.Panicln("Could not create trading position:", err.Error())
					}

					// reset buying threshold
					buyingThresholdMapping[currency] = currentPrice * (1 - sellingThreshold)
				}
			} else {
				log.Panicln("Account balance could not be retrieved")
			}
		}
	}
}

func startAccountBalanceTracker(exchange integrations.ExchangeIntegration) {
	log.Println("executor::startAccountBalanceTracker()")

	ticker := time.NewTicker(1 * time.Hour)

	for {
		select {
		case <-ticker.C:
			storeAccountBalance(exchange)
		}
	}
}

func startExecutionTimer(exchange integrations.ExchangeIntegration, quit <-chan bool) {
	log.Println("executor::startExecutionTimer()")

	ticker := time.NewTicker(1 * time.Minute)

	for {
		select {
		case <-ticker.C:
			handleExecution(exchange)
		case <-quit:
			log.Println("executor::startExecutionTimer()::stop")
			ticker.Stop()
			return
		}
	}
}

func storeAccountBalance(exchange integrations.ExchangeIntegration) {
	balance := exchange.GetAccountBalance()
	log.Println("executor::storeAccountBalance()", balance)

	if balance != nil {
		var totalAccountValue float64

		// calculate total account value
		for _, currency := range exchange.GetSupportedCurrencies() {
			reflection := reflect.ValueOf(balance)
			currencyBalance := reflect.Indirect(reflection).FieldByName(currency)

			if currencyBalance.Float() > 0 {
				if snapshot := exchange.GetCurrencySnapshot(currency); snapshot != nil {
					totalAccountValue += snapshot.Current * currencyBalance.Float()
				}
			}
		}

		totalAccountValue += balance.EUR
		log.Printf("Total account value: %f EUR", totalAccountValue)

		// store in database
		if err := db.StoreAccountValue(totalAccountValue); err != nil {
			log.Println("Could not store account value to database", err)
		}
	} else {
		log.Panicln("Could not retrieve account balance")
	}
}

func executePurchaseOrder(exchange integrations.ExchangeIntegration, currency string, totalPrice float64, unitPrice float64) *integrations.Order {
	log.Println("executor::executePurchaseOrder()")

	var orderAmount = totalPrice / unitPrice

	if order := exchange.CreateBuyOrder(currency, orderAmount, unitPrice); order != nil {
		newPosition := db.TradingPosition{
			Currency:     currency,
			Amount:       orderAmount,
			UnitPrice:    unitPrice,
			Value:        orderAmount,
			Timestamp:    time.Now().Unix(),
			RelatedOrder: order.ID,
			OrderType:    "B"}

		if addPositionErr := db.AddTradingPosition(&newPosition); addPositionErr != nil {
			log.Panicln("Could not create trading position. Potential data inconsistencies.")
		}

		return order
	}

	log.Println("Could not create purchase order.")
	return nil
}

func executeSalesOrder(exchange integrations.ExchangeIntegration, position db.TradingPosition, unitPrice float64) *integrations.Order {
	log.Println("executor::executeSalesOrder()")

	var order *integrations.Order
	if order = exchange.CreateSellOrder(position.Currency, position.Amount, unitPrice); order != nil {
		if err := db.UpdateTradingPosition(position.ID, order.ID, "S"); err != nil {
			log.Panicln("Could not update trading position after sell order has been created")
		}
		return nil
	}

	return order
}
