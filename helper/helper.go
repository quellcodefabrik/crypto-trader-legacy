package helper

// IsElementInArray returns true of the given element is in the
// also given array of string values.
func IsElementInArray(element string, list []string) bool {
	for _, listItem := range list {
		if listItem == element {
			return true
		}
	}
	return false
}
