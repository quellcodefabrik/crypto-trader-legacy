package helper

import (
	"testing"
)

var mockArray = []string{
	"test1",
	"test2",
	"test3"}

func TestIsElementInArray(t *testing.T) {
	var result bool

	result = IsElementInArray("test5", mockArray)
	if result == true {
		t.Errorf("The element is not an element of the array, got: %t, want: %t.",
			result, false)
	}

	result = IsElementInArray("test2", mockArray)
	if result == false {
		t.Errorf("The element is an element of the array, got: %t, want: %t.",
			result, true)
	}
}
