package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"

	db "./database"
	"./executor"
	"./integrations"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"github.com/mitchellh/mapstructure"
	"github.com/rs/cors"
)

const proxy = ""
const secret = "secret"

var emergencyStop = make(chan string, 1)
var bitstampIntegration integrations.Bitstamp

// User describes the user data necessary for authentication.
type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// JwtToken describes the response message containing the
// JWT token after a successful authentication.
type JwtToken struct {
	Token string `json:"token"`
}

// Exception contains the message describing the failure
type Exception struct {
	Message string `json:"message"`
}

// TokenData describes the data structure used for storing
// data in an authentication token
type TokenData struct {
	Username string
	Validity string
}

var users []User

// stores a mapping between the username and a valid token
// after the user logged out or the system has been restarted,
// this mapping will be gone and the user has to authentcate
// himself once more
var userTokenMap = make(map[string]string)

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.RequestURI)
		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}

func validationMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("trader::validationMiddleware()::HandlerFunc")

		if authorizationToken := r.Header.Get("x-access-token"); authorizationToken != "" {
			token, error := jwt.Parse(authorizationToken, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("There was an error")
				}
				return []byte(secret), nil
			})

			if error != nil {
				w.WriteHeader(http.StatusBadRequest)
				json.NewEncoder(w).Encode(Exception{Message: error.Error()})
				return
			}

			var tokenData TokenData
			mapstructure.Decode(token.Claims.(jwt.MapClaims), &tokenData)

			if token.Valid {
				if val, ok := userTokenMap[tokenData.Username]; ok && val == token.Raw {
					context.Set(r, "user", token.Claims)
					next(w, r)
				} else {
					w.WriteHeader(http.StatusUnauthorized)
					json.NewEncoder(w).Encode(Exception{Message: "Authorization token expired"})
					return
				}
			} else {
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(Exception{Message: "Invalid authorization token"})
			}
		} else {
			w.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(w).Encode(Exception{Message: "An authorization header is required"})
		}
	})
}

// cancelOrder cancels the order associated with the position given by the position ID.
func cancelOrder(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	positionID, err := strconv.Atoi(vars["id"])

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(Exception{Message: "Route parameter must represent an integer"})
		return
	}

	position, err := db.GetTradingPosition(int64(positionID))

	if err != nil {
		log.Println("Could not retrieve position from database")
		w.WriteHeader(http.StatusNotFound)
	} else {
		if position.RelatedOrder == 0 {
			log.Println("Order has been already executed")
			w.WriteHeader(http.StatusBadRequest)
		} else {
			result := bitstampIntegration.CancelOrder(position.RelatedOrder)

			if result == true {
				if positionDeleteErr := db.RemoveTradingPosition(position.ID); positionDeleteErr != nil {
					log.Panicln("Could not delete position from database. Potential data corruption.")
				}
				w.WriteHeader(http.StatusOK)
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(Exception{Message: "Could not cancel order on exchange platform."})
			}
		}
	}
}

// getPositions returns all available position that are currently
// held on any crypto investment platform.
func getPositions(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	if positions, err := db.GetTradingPositions(""); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(Exception{Message: err.Error()})
	} else {
		json.NewEncoder(w).Encode(positions)
	}
}

// getHistory returns all account value items that have been created
// so far. Optionally it can be limited to a data range.
func getHistory(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	if history, err := db.GetHistory(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(Exception{Message: err.Error()})
	} else {
		json.NewEncoder(w).Encode(history)
	}
}

// login authenticates the user and returns an authentication token
// which must be then included in each subsequent call.
func login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var user User
	_ = json.NewDecoder(r.Body).Decode(&user)

	for _, item := range users {
		if user.Username == item.Username && user.Password == item.Password {
			token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
				"username": user.Username,
				"validity": "always",
			})
			tokenString, error := token.SignedString([]byte(secret))
			if error != nil {
				fmt.Println(error)
			}
			userTokenMap[user.Username] = tokenString
			json.NewEncoder(w).Encode(JwtToken{Token: tokenString})
			return
		}
	}

	w.WriteHeader(http.StatusUnauthorized)
	json.NewEncoder(w).Encode(Exception{Message: "Invalid username or password"})
}

// Logout invalidates the auth token so that the user cannot use the
// same auth token anymore and is forced to login again.
func logout(w http.ResponseWriter, r *http.Request) {
	tokenData := context.Get(r, "user")
	var token TokenData
	mapstructure.Decode(tokenData.(jwt.MapClaims), &token)
	delete(userTokenMap, token.Username)
	w.WriteHeader(http.StatusOK)
}

// sellPosition offers the position given by the position ID for sale.
func sellPosition(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	positionID, err := strconv.Atoi(vars["id"])

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(Exception{Message: "Route parameter must represent an integer"})
		return
	}

	position, err := db.GetTradingPosition(int64(positionID))

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(Exception{Message: "Could not retrieve position from database."})
	} else {
		if position.RelatedOrder != 0 {
			w.WriteHeader(http.StatusMethodNotAllowed)
			json.NewEncoder(w).Encode(Exception{Message: "You cannot sell a position that has not yet been confirmed. Cancel the order first."})
			return
		}

		if currencySnapshot := bitstampIntegration.GetCurrencySnapshot(position.Currency); currencySnapshot != nil {
			sellOrder := bitstampIntegration.CreateSellOrder(position.Currency, position.Amount, currencySnapshot.Current)

			if sellOrder == nil {
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(Exception{Message: "Sell order creation failed."})
			} else {
				// mark position as up for sale by setting the relatedOrder property
				if updatePositionErr := db.UpdateTradingPosition(position.ID, sellOrder.ID, "S"); updatePositionErr != nil {
					log.Panicln("Could not update position in database. Potential data corruption.")
				}
				w.WriteHeader(http.StatusOK)
			}
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(Exception{Message: "Could not get currency snapshot."})
		}
	}
}

// stopTrading stops all trading activity until the next restart
// of the application. Cannot be undone over an API call.
func stopTrading(w http.ResponseWriter, r *http.Request) {
	emergencyStop <- "stop"
	w.WriteHeader(http.StatusOK)
}

func main() {
	log.SetOutput(os.Stdout)
	log.Print("Go server starting...")

	if proxy != "" {
		proxyURL, _ := url.Parse(proxy)
		http.DefaultTransport = &http.Transport{Proxy: http.ProxyURL(proxyURL)}
	}

	db.Init()

	bitstampIntegration := integrations.Bitstamp{}
	bitstampIntegration.Init()

	go executor.Init(&bitstampIntegration, emergencyStop)

	users = append(users, User{Username: "sascha", Password: "test"})
	users = append(users, User{Username: "mareike", Password: "test"})

	router := mux.NewRouter()

	router.HandleFunc("/auth/login", login).Methods("POST")
	router.HandleFunc("/auth/logout", validationMiddleware(logout)).Methods("POST")
	router.HandleFunc("/positions/{id}", validationMiddleware(sellPosition)).Methods("DELETE")
	router.HandleFunc("/positions/{id}/order", validationMiddleware(cancelOrder)).Methods("DELETE")
	router.HandleFunc("/stop", validationMiddleware(stopTrading)).Methods("POST")
	router.HandleFunc("/positions", validationMiddleware(getPositions)).Methods("GET")
	router.HandleFunc("/history", validationMiddleware(getHistory)).Methods("GET")

	router.Use(loggingMiddleware)

	corsHandler := cors.New(cors.Options{
		AllowedHeaders:   []string{"*"},
		AllowedOrigins:   []string{"http://localhost:8080"},
		AllowCredentials: false})

	handler := corsHandler.Handler(router)

	log.Fatal(http.ListenAndServe(":8205", handler))
}
