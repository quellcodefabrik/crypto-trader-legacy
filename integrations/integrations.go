package integrations

import (
	"sync"
	"time"
)

// ExchangeIntegration defines all functions an exchange integration must support
type ExchangeIntegration interface {
	CancelOrder(orderID int64) bool
	CreateBuyOrder(currency string, amount float64, price float64) *Order
	CreateSellOrder(currency string, amount float64, price float64) *Order
	GetAccountBalance() *AccountBalance
	GetCurrencySnapshot(currency string) *CurrencySnapshot
	GetOpenOrders(currency string) ([]Order, error)
	GetSupportedCurrencies() []string
	HasFreeRequestSlot() bool
	Init()
}

// TimeSlot helps to only access the exchange platform in defined timeslots
type TimeSlot struct {
	mutex    sync.Mutex
	interval int64
	last     int64
}

// Init initializes the Timeslot struct by setting its interval value
func (s *TimeSlot) Init(interval int64) {
	s.interval = interval
}

// IsFree checks if a new timeslot for accessing the platform is available
func (s *TimeSlot) IsFree() bool {
	s.mutex.Lock()

	if s.last > 0 && s.last < time.Now().Unix()-s.interval {
		s.mutex.Unlock()
		return false
	}

	s.last = time.Now().Unix()
	s.mutex.Unlock()
	return true
}

// AccountBalance contains the number of units per Currency
type AccountBalance struct {
	BCH float64 `json:"bch_balance,string"`
	BTC float64 `json:"btc_balance,string"`
	ETH float64 `json:"eth_balance,string"`
	EUR float64 `json:"eur_balance,string"`
	LTC float64 `json:"ltc_balance,string"`
	XRP float64 `json:"xrp_balance,string"`
}

// APIAccess defines the credentials for an integrated platform
type APIAccess struct {
	APIKey     string
	APISecret  string
	CustomerID string
}

// CurrencySnapshot contains all currency characteristics for one currency
type CurrencySnapshot struct {
	ID        int64
	Low       float64 `json:"low,string"`
	High      float64 `json:"high,string"`
	Current   float64 `json:"last,string"`
	Timestamp int64   `json:"timestamp,string"`
	Average   float64 `json:"vwap,string"`
}

// Order contains open order data
type Order struct {
	ID        int64   `json:"id,string"`
	Timestamp int64   `json:"datetime,string"`
	Type      int     `json:"type,string"`
	Price     float64 `json:"price,string"` // unit price
	Amount    float64 `json:"amount,string"`
}

// IsBuyingOrder returns true if it is a buying order
func (order *Order) IsBuyingOrder() bool {
	return order.Type == 0
}

// IsSellingOrder returns true if it is a selling order
func (order *Order) IsSellingOrder() bool {
	return order.Type == 1
}
