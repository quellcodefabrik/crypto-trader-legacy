package integrations

// This file is not tracked by Git anymore. To track it again do the following
// git update-index --no-assume-unchanged integrations/integrations.config.go

var apiAccessData = APIAccess{
	APIKey:     "",
	APISecret:  "",
	CustomerID: ""}

var testAPIAccessData = APIAccess{
	APIKey:     "apiKeyDEMO",
	APISecret:  "apiSecretDEMO",
	CustomerID: "12345"}
