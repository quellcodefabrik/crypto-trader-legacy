package database

import (
	"fmt"
	"log"
	"time"
)

// Transaction reflects the transaction made on a trading platform
type Transaction struct {
	ID        int
	Timestamp time.Time
	Type      string
	Currency  int
	Amount    float64
	Value     float64
}

// GetTransactions returns all transactions for the given
// currency or all transactions if no currency is given.
func GetTransactions(currency *CryptoCurrency) ([]Transaction, error) {
	log.Println("database::GetTransactions()")

	queryString := "SELECT * FROM Transaction"

	if currency != nil {
		queryString += " WHERE currencyId=" + currency.Token
	}

	stmtOut, err := db.Prepare(queryString)
	if err != nil {
		log.Println("Error on preparing database statement.")
		return nil, err
	}
	defer stmtOut.Close()

	var transactions = make([]Transaction, 0)

	rows, err := stmtOut.Query()
	if err != nil {
		log.Println("Error on statement execution.")
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		var transaction Transaction

		if err := rows.Scan(
			&(transaction.ID), &(transaction.Timestamp), &(transaction.Type),
			&(transaction.Currency), &(transaction.Amount), &(transaction.Value)); err != nil {
			return nil, err
		}

		transactions = append(transactions, transaction)
	}

	if rows.Err() != nil {
		log.Println("Error on iterating over rows.")
		return nil, err
	}

	return transactions, nil
}

// StoreTransaction stores the given transaction struct in the database taking into
// account the currency reference under which it is known in the connected
// currency exchange platform.
func StoreTransaction(currencyToken string, transaction *Transaction) error {
	log.Println("database::StoreTransaction()")

	insert, err := db.Prepare("INSERT INTO Transaction (timestamp, currency, " +
		"type, amount, value) VALUES( FROM_UNIXTIME( ? ), ?, ?, ?, ? )")

	if err != nil {
		return err
	}
	defer insert.Close()

	currency := getCryptoCurrencyByToken(currencyToken, availableCurrencies)

	if currency == nil {
		return &Error{
			fmt.Sprintf("Crypto-Currency '%s' is unkown. "+
				"Cannot insert transaction into database.", currencyToken)}
	}

	timestamp := transaction.Timestamp.Unix()
	_, err = insert.Exec(timestamp, currency.Token, transaction.Type,
		transaction.Amount, transaction.Value)
	return err
}
