package database

import (
	"fmt"
	"log"
)

// Parameter reflects one configuration parameter
type Parameter struct {
	Key   string `json:"key,string"`
	Value string `json:"value,string"`
}

// GetParameter returns the value of one specific configuration key.
func GetParameter(key string) string {
	log.Println("database::GetParameter()")

	queryStatement := fmt.Sprintf("SELECT * FROM Settings WHERE `key`='%s'", key)

	stmtOut, err := db.Prepare(queryStatement)
	if err != nil {
		log.Println("Error on preparing database statement:", err)
		return ""
	}
	defer stmtOut.Close()

	var parameter Parameter
	err = stmtOut.QueryRow().Scan(&(parameter.Key), &(parameter.Value))

	if err != nil {
		log.Println("Error on statement execution:", err)
		return ""
	}

	return parameter.Value
}

// GetSettings returns all configuration parameters.
func GetSettings() ([]Parameter, error) {
	log.Println("database::GetSettings()")

	stmtOut, err := db.Prepare("SELECT * FROM Settings")
	if err != nil {
		log.Println("Error on preparing database statement:", err)
		return nil, err
	}
	defer stmtOut.Close()

	var settings = make([]Parameter, 0)

	rows, err := stmtOut.Query()
	if err != nil {
		log.Println("Error on statement execution:", err)
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		var parameter Parameter

		if err := rows.Scan(
			&(parameter.Key), &(parameter.Value)); err != nil {
			return nil, err
		}

		settings = append(settings, parameter)
	}

	if rows.Err() != nil {
		log.Println("Error on iterating over rows.")
		return nil, err
	}

	return settings, nil
}

// ChangeSetting adds a new settings or updates an existing one in
// case it already exists
func ChangeSetting(key string, value string) error {
	log.Println("database::ChangeSetting()")

	insert, err := db.Prepare("INSERT INTO Settings " +
		"VALUES ( ?, ? ) ON DUPLICATE KEY UPDATE `key` = ?, `value` = ? ")

	if err != nil {
		return err
	}
	defer insert.Close()

	_, err = insert.Exec(key, value, key, value)
	return err
}
