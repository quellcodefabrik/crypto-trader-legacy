package database

import (
	"log"
	"time"
)

// History stores a total account value to timestamp mapping.
type History struct {
	ID        int64
	Timestamp int64   `json:"timestamp,number"`
	Value     float64 `json:"value,number"`
}

// GetHistory returns all history points.
func GetHistory() ([]History, error) {
	log.Println("database::GetHistory()")

	stmtOut, err := db.Prepare("SELECT * FROM History")
	if err != nil {
		log.Println("Error on preparing database statement:", err)
		return nil, err
	}
	defer stmtOut.Close()

	var history = make([]History, 0)

	rows, err := stmtOut.Query()
	if err != nil {
		log.Println("Error on statement execution:", err)
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		var entry History
		var timestamp time.Time

		if err := rows.Scan(
			&(entry.ID), &(timestamp), &(entry.Value)); err != nil {
			return nil, err
		}

		entry.Timestamp = timestamp.Unix()

		history = append(history, entry)
	}

	if rows.Err() != nil {
		log.Println("Error on iterating over rows.")
		return nil, err
	}

	return history, nil
}

// StoreAccountValue adds a new entry into the History table
func StoreAccountValue(value float64) error {
	log.Println("database::StoreAccountValue()")

	insert, err := db.Prepare("INSERT INTO History (timestamp, value) " +
		"VALUES ( FROM_UNIXTIME( ? ), ? )")

	if err != nil {
		return err
	}
	defer insert.Close()

	_, err = insert.Exec(time.Now().Unix(), value)
	return err
}
