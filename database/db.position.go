package database

import (
	"fmt"
	"log"
	"time"
)

// TradingPosition reflects a trade on a trading platform
type TradingPosition struct {
	ID           int64   `json:"id,number"`
	Currency     string  `json:"currency"`
	Amount       float64 `json:"amount,number"`
	UnitPrice    float64 `json:"unitPrice,number"`
	Value        float64 `json:"value,number"`
	Timestamp    int64   `json:"timestamp,number"`
	RelatedOrder int64   `json:"relatedOrder,number"`
	OrderType    string  `json:"OrderType"`
}

// GetTradingPosition return the position with the given ID or nil
// if no position with this ID is available.
func GetTradingPosition(positionID int64) (*TradingPosition, error) {
	log.Println("database::GetTradingPosition()")

	stmtOut, err := db.Prepare(fmt.Sprintf("SELECT * FROM Position WHERE id=%d", positionID))
	if err != nil {
		log.Println("Error on preparing database statement:", err)
		return nil, err
	}
	defer stmtOut.Close()

	var position TradingPosition
	var timestamp time.Time
	err = stmtOut.QueryRow().Scan(&(position.ID), &(position.Currency), &(position.Amount), &(position.UnitPrice),
		&(position.Value), &(timestamp), &(position.RelatedOrder), &(position.OrderType))

	position.Timestamp = timestamp.Unix()

	if err != nil {
		log.Println("Error on statement execution:", err)
		return nil, err
	}

	return &position, nil
}

// GetTradingPositions returns all positions for the given
// currency or all positions if no currency is given.
func GetTradingPositions(currency string) ([]TradingPosition, error) {
	log.Println("database::GetTradingPositions() for", currency)

	queryString := "SELECT * FROM Position "

	if currency != "" {
		queryString += fmt.Sprintf("WHERE currency='%s'", currency)
	}

	stmtOut, err := db.Prepare(queryString)
	if err != nil {
		log.Println("Error on preparing database statement:", err)
		return nil, err
	}
	defer stmtOut.Close()

	var tradingPositions = make([]TradingPosition, 0)

	rows, err := stmtOut.Query()
	if err != nil {
		log.Println("Error on statement execution:", err)
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		var tradingPosition TradingPosition

		var timestamp time.Time
		if err := rows.Scan(
			&(tradingPosition.ID), &(tradingPosition.Currency), &(tradingPosition.Amount), &(tradingPosition.UnitPrice),
			&(tradingPosition.Value), &(timestamp), &(tradingPosition.RelatedOrder), &(tradingPosition.OrderType)); err != nil {
			return nil, err
		}

		tradingPosition.Timestamp = timestamp.Unix()
		tradingPositions = append(tradingPositions, tradingPosition)
	}

	if rows.Err() != nil {
		log.Println("Error on iterating over rows.")
		return nil, err
	}

	return tradingPositions, nil
}

// AddTradingPosition adds a new position item into the database as soon as the
// trade was executed successfully.
func AddTradingPosition(position *TradingPosition) error {
	log.Println("database::AddTradingPosition()")

	insert, err := db.Prepare("INSERT INTO Position (timestamp, currency, amount, unitPrice, value, relatedOrder, orderType) " +
		"VALUES( FROM_UNIXTIME( ? ), ?, ?, ?, ? )")

	if err != nil {
		return err
	}
	defer insert.Close()

	timestamp := time.Unix(position.Timestamp, 0)
	_, err = insert.Exec(timestamp, position.Currency, position.Amount, position.UnitPrice,
		position.Value, position.RelatedOrder, position.OrderType)
	return err
}

// RemoveTradingPosition removes the position item from the database as soon as
// it was sold successfully.
func RemoveTradingPosition(positionID int64) error {
	log.Println("database::RemoveTradingPosition()")

	deleteStatement, err := db.Prepare(`DELETE FROM Position WHERE id = ?`)

	if err != nil {
		return err
	}
	defer deleteStatement.Close()

	result, err := deleteStatement.Exec(positionID)
	rowsAffected, err := result.RowsAffected()

	if err != nil {
		return fmt.Errorf("mysql: could not get rows affected: %v", err)
	} else if rowsAffected != 1 {
		return fmt.Errorf("mysql: expected 1 row affected, got %d", rowsAffected)
	}
	return err
}

// UpdateTradingPosition updates the trading position in the database
func UpdateTradingPosition(positionID int64, relatedOrder int64, orderType string) error {
	log.Println("database::UpdateTradingPosition()")

	update, err := db.Prepare("UPDATE Position SET relatedOrder = ?, orderType = ? WHERE id = ?")

	if err != nil {
		return err
	}
	defer update.Close()

	_, err = update.Exec(relatedOrder, orderType, positionID)
	return err
}
